#!/usr/bin/python

from data_utils import get_formatted_data
from neural_net import *


def main():
    # Get normalized and formatted data
    Xtr, Ytr, Xval, Yval, Xte, Yte = get_formatted_data()

    # Constants
    input_size = 32 * 32 * 3
    num_classes = 10

    # Hyperparameteres
    hidden_size = 600
    num_iters = 2000
    batch_size = 300
    learning_rate = 1e-3
    reg = 0.5

    # Initialize the two layer net
    two_layer_net = TwoLayerNet(input_size, hidden_size, num_classes)

    # Train the network
    stats = two_layer_net.train(Xtr, Ytr, Xval, Yval,
                                num_iters=num_iters,
                                batch_size=batch_size,
                                learning_rate=learning_rate,
                                learning_rate_decay=0.95,
                                reg=reg, verbose=True)

    # Predict on different sets
    train_acc = (two_layer_net.predict(Xtr) == Ytr).mean()
    val_acc = (two_layer_net.predict(Xval) == Yval).mean()
    test_acc = (two_layer_net.predict(Xte) == Yte).mean()

    # Output
    print '\nTraining accuracy: ' + str(int(train_acc * 100)) + '%'
    print 'Validation accuracy: ' + str(int(val_acc * 100)) + '%'
    print 'Testing accuracy: ' + str(int(test_acc * 100)) + '%'


if __name__ == "__main__":
    main()
