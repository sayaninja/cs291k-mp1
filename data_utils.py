import cPickle as pickle
import numpy as np
import os
import sys

from os import path


def load_CIFAR_batch(filename):
    """ load single batch of cifar """
    with open(filename, 'rb') as f:
        datadict = pickle.load(f)
        X = datadict['data']
        Y = datadict['labels']
        X = X.reshape(10000, 3, 32, 32).transpose(0, 2, 3, 1).astype("float")
        Y = np.array(Y)
        return X, Y


def load_CIFAR10(ROOT):
    """ load all of cifar """
    xs = []
    ys = []
    for b in range(1, 6):
        f = os.path.join(ROOT, 'data_batch_%d' % (b,))
        X, Y = load_CIFAR_batch(f)
        xs.append(X)
        ys.append(Y)
    Xtr = np.concatenate(xs)
    Ytr = np.concatenate(ys)
    del X, Y
    Xte, Yte = load_CIFAR_batch(os.path.join(ROOT, 'test_batch'))
    return Xtr, Ytr, Xte, Yte


def get_formatted_data():
    if len(sys.argv) != 2:
        print 'Incorrect counter of arguments.'
        exit()

    dataset_folder_name = str(sys.argv[1])
    Xtr, Ytr, Xte, Yte = load_CIFAR10(path.join(dataset_folder_name, 'cifar-10-batches-py'))

    train_count = Xtr.shape[0]
    val_count = 1000
    test_count = Xte.shape[0]

    train_mask = range(train_count)
    Xtr = Xtr[train_mask]
    Ytr = Ytr[train_mask]

    val_mask = range(val_count)
    Xval = Xtr[val_mask]
    Yval = Ytr[val_mask]

    test_mask = range(test_count)
    Xte = Xte[test_mask]
    Yte = Yte[test_mask]

    # Normalizing by subtracting the mean image
    mean_image = np.mean(Xtr, axis=0)
    Xtr -= mean_image
    Xval -= mean_image
    Xte -= mean_image

    # Reshaping into rows
    Xtr = Xtr.reshape(train_count, -1)
    Xval = Xval.reshape(val_count, -1)
    Xte = Xte.reshape(test_count, -1)

    return Xtr, Ytr, Xval, Yval, Xte, Yte
